package CadenceGradle;

import com.uber.cadence.worker.Worker;
import com.uber.cadence.workflow.QueryMethod;
import com.uber.cadence.workflow.Workflow;
import com.uber.cadence.workflow.WorkflowMethod;

import CadenceGradle.GettingStartedActivityWorker.ReadWriteActivities;

public class GettingStarted {

    public interface ReadWriteWorkflow {
        @WorkflowMethod
        void startWorkflow(String filename);
        
        @QueryMethod
        int getCount();
    }

    public static class ReadWriteWorkflowImpl implements ReadWriteWorkflow {
    	
    	private final ReadWriteActivities activities = Workflow.newActivityStub(ReadWriteActivities.class);
        private int count = 0;

        @Override
        public void startWorkflow(String filename) {
            /*while (!"Bye".equals(greeting)) {
                activities.read(++count + ": " + greeting + " " + name + "!");
                String oldGreeting = greeting;
                Workflow.await(() -> !Objects.equals(greeting, oldGreeting));
            }*/
            boolean answer = activities.read(filename);
            activities.write(filename, answer);
        }


        @Override
        public int getCount() {
            return count;
        }
    }
    
    public static void main(String[] args) {
        Worker.Factory factory = new Worker.Factory("test-domain");
        Worker worker = factory.newWorker("WorkflowTaskList");
        worker.registerWorkflowImplementationTypes(ReadWriteWorkflowImpl.class);
        factory.start();
    }
}