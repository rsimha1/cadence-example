package CadenceGradle;

import com.uber.cadence.activity.ActivityMethod;
import com.uber.cadence.worker.Worker;

public class GettingStartedActivityWorker {
	
	public interface ReadWriteActivities {
		@ActivityMethod(scheduleToCloseTimeoutSeconds = 100)
	    boolean read(String filename);
		
		@ActivityMethod(scheduleToCloseTimeoutSeconds = 100)
	    void write(String filename, boolean answer);
	}
	
    public static void main(String[] args) {
        Worker.Factory factory = new Worker.Factory("test-domain");
        Worker worker = factory.newWorker("WorkflowTaskList");
        worker.registerActivitiesImplementations(new ReadWriteActivitiesImpl(System.out));
        factory.start();
    }
}


