package CadenceGradle;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import org.json.simple.JSONObject;

import CadenceGradle.GettingStartedActivityWorker.ReadWriteActivities;
import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import io.minio.errors.MinioException;



public class ReadWriteActivitiesImpl implements ReadWriteActivities {
    private final PrintStream out;

    public ReadWriteActivitiesImpl(PrintStream out) {
        this.out = out;
    }

    @Override
    public boolean read(String filename) {
    	boolean ans = false;
    	
		MinioClient minioClient =
			    MinioClient.builder()
			        .endpoint("http://127.0.0.1:9000")
			        .credentials("AKIAIOSFODNN7EXAMPLE", "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY")
			        .build();
		
		// get object given the bucket and object name
		try (InputStream stream = minioClient.getObject(
		  GetObjectArgs.builder()
		  .bucket("questions")
		  .object(filename)
		  .build()) ){
			
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
	        String line = null;
	        int count = 0;
	        
	        while ((line = reader.readLine()) != null) {
	        	line.replaceAll(" ", "");
				count += line.length();
				if(count > 20) {
					ans = true;
					break;
				}
	            
	        }
	        
		}catch(IOException e) {
			out.println(e);
		} catch (MinioException e) {
			// TODO Auto-generated catch block
			out.println(e);
		}catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			out.println(e);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			out.println(e);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			out.println(e);
		}
		
		out.println("Validation? " + ans);
		return ans;
		
		
    }
    
    @Override
    public void write(String filename, boolean answer) {
    	boolean ans = answer;
		
		MinioClient minioClient =
			    MinioClient.builder()
			        .endpoint("http://127.0.0.1:9000")
			        .credentials("AKIAIOSFODNN7EXAMPLE", "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY")
			        .build();
		
		// get object given the bucket and object name
		try {
			//Creating a JSONObject object
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("Validation", ans);
			FileWriter file = new FileWriter("output.json");
			file.write(jsonObject.toJSONString());
			file.close();
			
			minioClient.uploadObject(
				    UploadObjectArgs.builder()
				        .bucket("questions").object("validation").filename("output.json").build());
			
			out.println("Upload complete");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MinioException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (InvalidKeyException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalArgumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (NoSuchAlgorithmException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    }
}
