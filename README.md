# Cadence Example

To run the following example:
Download and run [Minio](https://github.com/minio/minio)
- Create a bucket called "questions"
- Upload a file with text that contains 20 or more characters
- Name this file "question1"
Download and start the [Cadence](https://cadenceworkflow.io/) server using docker

Clone the Cadence Example repository
Open up the project in an IDE (ex. IntelliJ or Eclipse)
Run the GetStarted class and GetStartedActivityWorker class

Run this command in terminal:
docker run --network=host --rm ubercadence/cli:master --do test-domain workflow start  --workflow_id "ReadWriteWorkflow" --tasklist WorkflowTaskList --workflow_type ReadWriteWorkflow::startWorkflow --execution_timeout 3600 --input \\"question1\\"

Your minio bucket should contain a file named "validation" with a JSON format of an output of true or false.

